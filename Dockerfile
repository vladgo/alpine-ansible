# pull base image
FROM docker:git

ENV KEYHOST=https://sbt-qa-jenkins.sigma.sbrf.ru:9009/files/certs

COPY ./docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

RUN set -x \
    && apk update \
    && apk add --no-cache git py-requests curl xz bash sshpass py2-pip \
    && apk add gcc libffi-dev musl-dev openssl-dev perl python python-dev libsodium-dev \
    && SODIUM_INSTALL=system pip install pynacl \
    && pip install ansible ansible-lint docker-py

RUN if curl -k -m 3 $KEYHOST/sigma/SberbankExternalCA2.crt; then \
  curl -sk $KEYHOST/sigma/SberbankExternalCA2.crt -o  /usr/local/share/ca-certificates/SberbankExternalCA2.crt \
  && curl -sk $KEYHOST/sigma/SberbankRootCA.crt -o /usr/local/share/ca-certificates/SberbankRootCA_Sigma.crt \
  && curl -sk $KEYHOST/alpha/SberbankEnterpriseCA2.cer -o /usr/local/share/ca-certificates/SberbankEnterpriseCA2.crt \
  && curl -sk $KEYHOST/alpha/SberbankRootCA.cer -o /usr/local/share/ca-certificates/SberbankRootCA_Alpha.crt \
  && update-ca-certificates; fi

ENTRYPOINT ["docker-entrypoint.sh"]
